/**
 * Created by cristian.jora on 28.09.2016.
 */
import Registration from './../lib/registration'

describe("Registration",()=>{
   var reg={};
   before((done)=>{
      reg=new Registration();
      done();
   })
  describe('a valid application',()=>{
    var regResult={};
    before((done)=>{
        reg.applyForMembership({email:"joracristi11@gmail.com",password:"test",confirm:"test"},(err,result)=>{
          regResult=result;
          done();
        })
    })
    it('is successful',()=>{
        regResult.success.should.equal(true);
    })
    it('creates a user',()=>{
      regResult.user.should.be.defined;
    })
    it('creates a log entry',()=>{
        regResult.log.should.be.defined;
    })
    it('offers a welcome message',()=>{
     regResult.message.should.equal("Welcome!")
    })
     it("increments signInCount",()=>{
       regResult.user.should.be.defined;
       regResult.user.signInCount.should.equal(1)
     })
  })

    describe('an empty or null email',()=>{
        it('is not successful')
        it('tells the user that email is required')
    })

    describe('empty or null password',()=>{
        it('is not successful')
        it('tells the user that password is required')
    })

    describe('password and confirm mismatch',()=>{
        it('is not successful')
        it('tells the user that password and confirm mismatch')
    })

    describe('email already exists',()=>{
        var regResult={};
        before((done)=>{
            reg.applyForMembership({email:"test2@gmail.com",password:"test",confirm:"test"},(err,result)=>{
                regResult=result;
                done();
            })
        })
        it('is not successful',()=>{
          regResult.success.should.equal(false);
        })
        it('tells the user that email already exists',()=>{
            regResult.success.should.equal(false);
            regResult.message.should.equal('This email is already taken');
        })
    })
})