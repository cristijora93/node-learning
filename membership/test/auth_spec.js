/**
 * Created by cristian.jora on 29.09.2016.
 */
import Authentication from './../lib/authentication'
import Registration from './../lib/registration'
describe('Authentication',()=>{
    var reg={};
    var auth={};
    before((done)=>{
        console.log(auth,"Passed credentials")
        auth=new Authentication();
        reg=new Registration();
        done();
    })
    describe('a valid login',()=>{
        var authResult={};
        before((done)=>{
            reg.applyForMembership({email:"joracristi@gmail.com",password:"test",confirm:"test"},(result)=>{

                auth.authenticate({email:"joracristi@gmail.com",password:"test"},(err,result)=>{
                   authResult=result;
                    done();
                })

            })
        })
        it('is successful',()=>{
            authResult.success.should.equal(true);
          //authResult.message.should.equal('Welcome!')
        })
        it('returns a user')
        it('creates a log entry')
        it('updates the signon dates')

    })

    describe('empty email',()=>{
        it('is not successful')
        it("returns a message saying 'Invalid login!")
    })

    describe('empty password',()=>{
        it('is not successful')
        it("returns a message saying 'Invalid login!")
    })

    describe("password doesn't match",()=>{
        it('is not successful')
        it("returns a message saying 'Invalid login!")
    })

    describe('email not found',()=>{
        it('is not successful')
        it("returns a message saying 'Invalid login!")
    })
})