/**
 * Created by cristian.jora on 28.09.2016.
 */
import should from 'should'
import User from './../models/user'

describe('User',()=>{
  describe('defaults',()=>{
    var user={};
    before(()=>{
      user=new User({email:"joracristi@gmail.com",username:"cristi"})
    })

    it("email is joracristi@gmail.com",()=>{
      user.email.should.equal("joracristi@gmail.com")
    })
    /*it("has an authentication token",()=>{
      user.authenticationToken.should.be.defined
    })*/
    it("has a created date",()=>{
        user.createdAt.should.be.defined
    })
    it("has a signInCount of 0",()=>{
        user.signInCount.should.equal(0)
    })
    it("has lastLogin",()=>{
      user.lastLoginAt.should.be.defined
    })
    it("has a username",()=>{
      user.username.should.be.defined
    })
  })
})