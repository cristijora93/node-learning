/**
 * Created by cristian.jora on 29.09.2016.
 */
var Emitter=require('events').EventEmitter
var util=require('util')
import UserSchema from './database/models/user'
import User from './models/user'
import Log from './models/log'
import LogSchema from './database/models/log'
import bcrypt from 'bcrypt-nodejs'
import guid from 'guid'

var AuthResult=(creds)=>{
    var result={
        creds:creds,
        success:false,
        message:"Invalid username or password",
        user:null,
        log:null
    }
    return result;
}
var Authentication = function(){
    Emitter.call(this) //pass this to event emitter so events can be used from this
    var continueWith=null;

    //validate credentials
    var validateCredentials=(authResult)=>{
        if(authResult.creds.email && authResult.creds.password){
          this.emit('creds-ok',authResult);
      }
      else{
          this.emit('invalid',authResult)
      }
    }
    //find user
    var findUser=(authResult)=>{
        UserSchema.find({where:{email:authResult.creds.email}}).then((result)=>{
          if(result){
              authResult.user=new User(result);
              this.emit('user-found',authResult)
          }
          else{
              this.emit('invalid',authResult)
          }
        })
    }
    //compare passwords
    var comparePassword=(authResult)=>{
        var matched=bcrypt.compareSync(authResult.creds.password,authResult.user.hashedPassword);
        if(matched){
            this.emit('password-accepted',authResult)
        }else{
            this.emit('invalid',authResult)
        }
    }
    //bump the stats
    var updateUserStats=(authResult)=>{
      var user=authResult.user;
        user.signInCount+=1;
        user.lastLoginAt=new Date();
        var updates={
            signInCount:user.signInCount,
            lastLoginAt:user.lastLoginAt
        }
        UserSchema.update(updates,{where:{id:user.id}}).then((result)=>{
          if(result){
              this.emit('stats-updated',authResult)
          }else{
              this.emit('invalid',authResult)
          }
        })

    }

    var addLogEntry=(authResult)=>{
        console.log(authResult.user, "Trying to login!!")
        var log=new Log({
            subject:"Authentication",
            entry: "Successfully logged in",
            userId:authResult.user.id,
        });
        log.id=guid.create().value
        LogSchema.create(log).then((result)=>{
            if(result){
                authResult.log=result;
                this.emit('log-created',authResult)
            }else{
                authResult.setInvalid('Log could not be saved in db')
                this.emit('invalid',authResult)
            }
        });
    }
    var loginSuccess=(authResult)=>{
        authResult.success=true;
        authResult.message="Welcome!";
        this.emit('authenticated',authResult)
        this.emit('completed',authResult)
        if(continueWith){

            continueWith(null,authResult)
        }
    }

    var loginFail=(authResult)=>{
        authResult.success=false;
        authResult.message=authResult.message;
        this.emit('not-authenticated',authResult)
        this.emit('completed',authResult)
        if(continueWith){
            continueWith(null,authResult)
        }
    }

    //happy
    this.on('login-received',validateCredentials)
    this.on('creds-ok',findUser)
    this.on('user-found',comparePassword)
    this.on('password-accepted',updateUserStats)
    this.on('stats-updated',addLogEntry)
    this.on('log-created',loginSuccess)

    //sad
    this.on('invalid',loginFail)
    this.authenticate=(creds,next)=>{
        continueWith=next;
        var authResult=new AuthResult(creds);
        this.emit('login-received',authResult)
    }
    return this;
}

util.inherits(Authentication,Emitter);
module.exports=Authentication;

