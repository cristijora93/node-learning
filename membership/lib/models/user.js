/**
 * Created by cristian.jora on 28.09.2016.
 */
import assert from 'assert'
var User = (args)=>{
    assert.ok(args.email,"Email is required")

    var user={}
    user.id=args.id;
    user.email=args.email;
    user.username=args.username;
    user.password=args.password;
    user.createdAt=args.createdAt || new Date();
    user.signInCount=args.signInCount || 0;
    user.lastLoginAt=args.lastLoginAt || new Date();
    user.authenticationToken=args.authenticationToken || null;
    user.hashedPassword=args.hashedPassword || null;
    return user;

}

module.exports=User;

