/**
 * Created by cristian.jora on 28.09.2016.
 */
var Application = (args)=>{

    var app={}
    app.email=args.email;
    app.username=args.username;
    app.password=args.password;
    app.confirm=args.confirm;
    app.status="pending";
    app.message=null;
    app.user=null;
    app.isValid=()=>{
      return app.status=="validated"
    }

    app.isInvalid=()=>{
        return !app.isValid();
    }

    app.setInvalid=(message)=>{
      app.status="invalid";
      app.message=message;
    }

    app.validate=()=>{
        app.status="validated";
    }
    return app;
}

module.exports=Application;