/**
 * Created by cristian.jora on 29.09.2016.
 */
import assert from 'assert'

var Log=(args)=>{
  console.log(args," LOG ARGUMENTS_-----------------------------------")
  assert.ok(args.subject && args.entry && args.userId, "Need subject entry and userId")

    var log={};
    log.subject=args.subject;
    log.entry=args.entry;
    log.createdAt=new Date();
    log.userId=args.userId;

  return log;
}

module.exports=Log;