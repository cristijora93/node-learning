/**
 * Created by cristian.jora on 30.09.2016.
 */
var Emitter=require('events').EventEmitter
var util=require('util')
import Authentication from './authentication'
import Registration from './registration'

var Membership = function(){
    Emitter.call(this);
    this.authenticate=(email,password,next)=>{
        var auth=new Authentication()
        auth.on('authenticated',(authResult)=>{
          this.emit('authenticated',authResult)
        })
        auth.on('not-authenticated',(authResult)=>{
            this.emit('not-authenticated',authResult)
        })
        auth.authenticate({email:email,password:password},next)
    }
    this.register=(email,password,confirm,next)=>{
        var reg=new Registration()
        reg.on('registered',(authResult)=>{
            this.emit('registered',authResult)
        })
        reg.on('not-registered',(authResult)=>{
            this.emit('not-registered',authResult)
        })
        reg.register({email:email,password:password},next)
    }
}

util.inherits(Membership,Emitter);
module.exports=Membership;