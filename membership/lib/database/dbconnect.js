/**
 * Created by cristian.jora on 28.09.2016.
 */
import Sequelize from 'sequelize'

var db = new Sequelize('NodeTest','sa','Corebuild123', {
    host:'localhost',
    port:'1433',
    dialect:'mssql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
})

module.exports=db;