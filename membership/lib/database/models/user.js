/**
 * Created by cristian.jora on 28.09.2016.
 */
import db from '../dbconnect'
import Sequelize from 'sequelize'

var UserSchema = db.define("User", {
    id: {type:Sequelize.STRING,unique:true,primaryKey: true},
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    createdAt:Sequelize.DATE,
    signInCount:Sequelize.INTEGER,
    lastLoginAt:Sequelize.DATE,
    authenticationToken:Sequelize.STRING,
    hashedPassword:Sequelize.STRING

});
UserSchema.sync();

module.exports=UserSchema;