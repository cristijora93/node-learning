/**
 * Created by cristian.jora on 29.09.2016.
 */
import db from '../dbconnect'
import Sequelize from 'sequelize'

var LogSchema = db.define("Log", {
    id: {type:Sequelize.STRING,unique:true,primaryKey: true},
    subject: Sequelize.STRING,
    entry: Sequelize.TEXT,
    userId:Sequelize.STRING,
});

LogSchema.sync();

module.exports=LogSchema;