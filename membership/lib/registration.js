/**
 * Created by cristian.jora on 28.09.2016.
 */
import User from './models/user'
import Log from './models/log'
import Application from './models/application'
import guid from 'guid'
import bcrypt from 'bcrypt-nodejs'
import UserSchema from './database/models/user'
import LogSchema from './database/models/log'
var Emitter=require('events').EventEmitter
var util=require('util')

var RegResult =()=>{
    var result={
        success:false,
        message:null,
        user:null
    }
    return result;
}
var Registration = function(){
    Emitter.call(this) //pass this to event emitter so events can be used from this
    var continueWith=null;

    var validateInputs=(app)=>{

        if(!app.email || !app.password){
            app.setInvalid("Email and password are required")
            this.emit("invalid",app);
        }else if(app.password !==app.confirm){
            app.setInvalid("Passwords don't match")
            this.emit("invalid",app);
        }
        else{
            app.validate();
            this.emit("validated",app);
        }
    }

    var checkIfUserExists=(app)=>{
        UserSchema.find({where:{email:app.email}}).then((result)=>{
          if(result){
              app.setInvalid('This email is already taken')
              this.emit('invalid',app)
          }else{
              this.emit('user-doesnt-exist',app)
          }
        })
    }

    var saveUser=(app)=>{
        var newGuid=guid.create().value;
        var user=new User(app);
        user.id=newGuid;
        user.signInCount=1;
        user.hashedPassword=bcrypt.hashSync(user.password)
        UserSchema.create(user).then((result)=>{
          if(result){
              app.user=result;
              this.emit('user-created',app)
          }else{
              app.setInvalid('User could not be saved in db')
              this.emit('invalid',app)
          }
        })
    }

    var addLogEntry=(app)=>{
        var log=new Log({
            subject:"Registration",
            userId:app.user.id,
            entry: "Successfully Registered"
        });
        log.id=guid.create().value
        LogSchema.create(log).then((result)=>{
           if(result){
               app.log=result;
               this.emit('log-created',app)
           }else{
               app.setInvalid('Log could not be saved in db')
               this.emit('invalid',app)
           }
        });
    }

    this.applyForMembership =(args,next)=>{
        continueWith=next;
        var app=new Application(args);
        if(app.isValid()){
            //success
        }
        this.emit('application-received',app)
    }

    var registrationSuccess=(app)=>{
        var regResult= new RegResult();
        regResult.success=true;
        regResult.message='Welcome!'
        regResult.user=app.user;
        regResult.log=app.log;
        this.emit('registered',regResult)
        if(continueWith){
            continueWith(null,regResult); //pass the callback further
        }
    }
    var registrationFail=(app)=>{
        var regResult= new RegResult();
        regResult.success=false;
        regResult.message=app.message;
        this.emit('not-register',regResult)
        if(continueWith){
            continueWith(null,regResult); //pass the callback further
        }
    }
    //listen to events
    this.on('application-received',validateInputs);
    this.on('validated',checkIfUserExists);
    this.on('user-doesnt-exist',saveUser);
    this.on('user-created',addLogEntry);
    this.on('log-created',registrationSuccess);

    this.on('invalid',registrationFail);

    return this;
}

util.inherits(Registration,Emitter)
module.exports=Registration;